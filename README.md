# Politiskt blogg med hugo

Det här är ett exempel med politisk blogg med hugo.

Följande tema använt är [PaperMod](https://themes.gohugo.io/themes/hugo-papermod/) av Aditya Telange som är under MIT-licens.

Alla filer läggs under:

* **content** - statiska sidor och blogginlägg
* **static** - bilder