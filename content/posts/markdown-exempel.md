+++
title = 'Litet blogginlägg med Markdown'
date = 2023-10-30T21:25:21+01:00
draft = false
+++

Så här ser Markdown ut! För det allra mesta som vanligt text. Men om man vill formatera på något sätt (som kursiv stil eller fet text) eller lägga till listor eller bilder så får man skriva enkla koder.

# Huvudrubrik

En rad som startar med `#` blir rubriknivå 1.

## Rubriknivå 2

En rad som startar med `##` blir rubriknivå 2.

## Formattering

Här kommer lite formatering med **fet text**, *kursiv text* och [här är en länk till Konstellationens hemsida](https://konstellationen.org). Här är en lista:

* En punkt
* Ännu en punkt
* Och så en till

Man kan också numrera:

1. Första punkten
2. Andra punkten
3. Tredje punkten

## Bilder

Antingen så kan man länka till en bild någon annanstans. Här är ett exempel på länk till Konstellationens logga:

![Konstellationens logga](https://konstellationen.org/images/logo_with_text.png).

Eller så länkar man till en bild som finns på själva sidan men då måste man se till att den redan finns. Jag råkar veta att det finns en fil under `/static/traffic.jpg` så då länkar jag den här med bara `/traffic.jpg` (eftersom katalogen `/static` är den katalog Hugo tittar i efter bilder):

![En bild på trafik](/traffic.jpg).

Och med det så kan göra väldigt mycket.