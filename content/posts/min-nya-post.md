+++
title = 'Min nya post'
date = 2023-10-30T22:05:21+01:00
draft = false
+++

Här är min nya post! Det första jag vill göra är att tipsa om att [Bli medlem i Kamratdataföreningen Konstellationen](https://konstellationen.org/bli-medlem/).

Det andra jag vill göra är att göra en punktlista:

* Punkt ett
* Punkt två

Ja, nu är jag färdig!